package com.vadim.repository;

import com.vadim.model.entity.TodoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.Optional;

public interface TodoRepository extends JpaRepository<TodoEntity, String> {
    List<TodoEntity> findAllByOwnerEmail(String ownerEmail);
}
