package com.vadim.hadndler;

import com.vadim.hadndler.exception.IncorrectUserException;
import com.vadim.model.entity.TodoEntity;
import com.vadim.model.protocol.TodoDTO;
import com.vadim.repository.TodoRepository;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class TodoHandler {

    private final TodoRepository todoRepository;

    public TodoHandler(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public Flux<TodoDTO> getAllTodoFor(String email) {
        return Flux.fromIterable(todoRepository.findAllByOwnerEmail(email).stream()
                .map(todoEntity -> TodoDTO.builder()
                        .done(todoEntity.isDone())
                        .text(todoEntity.getText())
                        .id(todoEntity.getId())
                        .build())
                .collect(Collectors.toList()));
    }

    public Mono<TodoDTO> create(TodoDTO create, String email) {
        TodoEntity toSave = TodoEntity.builder()
                .done(create.isDone())
                .text(create.getText())
                .ownerEmail(email)
                .build();
        return Mono.justOrEmpty(
                Optional.of(todoRepository.save(toSave))
                        .map(entityToDto()));
    }


    private Function<TodoEntity, TodoDTO> entityToDto() {
        return (entity) -> TodoDTO.builder()
                .text(entity.getText())
                .id(entity.getId())
                .done(entity.isDone())
                .build();
    }

    public Mono<TodoDTO> update(TodoDTO create, String email) {
        return Mono.justOrEmpty(todoRepository.findById(create.getId())
                .map(old -> {
                    if (!old.getOwnerEmail().equals(email))
                        throw new IncorrectUserException();
                    old.setDone(create.isDone());
                    old.setText(create.getText());
                    return todoRepository.save(old);
                })
                .map(entityToDto()));

    }
}
