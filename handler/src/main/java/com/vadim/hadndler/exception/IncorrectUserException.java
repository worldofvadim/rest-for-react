package com.vadim.hadndler.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "User update incorrect item")
public class IncorrectUserException extends RuntimeException {
}
