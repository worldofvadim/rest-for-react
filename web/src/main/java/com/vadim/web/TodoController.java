package com.vadim.web;

import com.vadim.hadndler.TodoHandler;
import com.vadim.model.protocol.TodoDTO;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController("/api/todo")
public class TodoController {

    private final TodoHandler todoHandler;

    public TodoController(TodoHandler todoHandler) {
        this.todoHandler = todoHandler;
    }

    @GetMapping
    public Flux<TodoDTO> getAll(@AuthenticationPrincipal String email){
        return todoHandler.getAllTodoFor(email);
    }

    @PostMapping
    public Mono<TodoDTO> createTodo(@RequestBody TodoDTO create, @AuthenticationPrincipal String email){
        return todoHandler.create(create,email);
    }

    @PutMapping
    public Mono<TodoDTO> updateDto(@RequestBody TodoDTO create, @AuthenticationPrincipal String email){
        return todoHandler.update(create, email);
    }
}
