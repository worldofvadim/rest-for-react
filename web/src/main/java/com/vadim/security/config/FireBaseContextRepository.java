package com.vadim.security.config;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.web.server.context.ServerSecurityContextRepository;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

@Component
public class FireBaseContextRepository implements ServerSecurityContextRepository {

    @Override
    public Mono<Void> save(ServerWebExchange serverWebExchange, SecurityContext securityContext) {
        // Don't know yet where this is for.
        return null;
    }

    @PostConstruct
    public void configureFirebase() throws IOException {
        FileInputStream serviceAccount =
                new FileInputStream("/home/vadim/googleKey.json");

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl("https://test-cbf1b.firebaseio.com")
                .build();

        FirebaseApp.initializeApp(options);
    }

    @Override
    public Mono<SecurityContext> load(ServerWebExchange serverWebExchange) {
        // JwtAuthenticationToken and GuestAuthenticationToken are custom Authentication tokens.
        Optional<String> authorization = serverWebExchange.getRequest().getHeaders().get("Authorization").stream().findFirst();
        if(!authorization.isPresent())
            return Mono.empty();

        // idToken comes from the HTTP Header
        FirebaseToken decodedToken;
        try {
            decodedToken = FirebaseAuth.getInstance().verifyIdToken(authorization.get());
        } catch (FirebaseAuthException e) {
            return Mono.empty();
        }

        AbstractAuthenticationToken user = new AbstractAuthenticationToken(Arrays.asList(new SimpleGrantedAuthority("USER"))) {
            @Override
            public Object getCredentials() {
                return decodedToken.getEmail();
            }

            @Override
            public Object getPrincipal() {
                return decodedToken.getEmail();
            }
        };
        user.setAuthenticated(true);
        return Mono.just(new SecurityContextImpl(user));
    }
}
